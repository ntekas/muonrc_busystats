#!/usr/bin/env bash

tstart=$4
tstop=$5
step=`expr $5 - $4 + 1`
#step=1209600
#step=2000000

if [ -f $1.txt ]
then
    rm -rf tmp.txt
    awk '{print $1}' $1.txt > tmp.txt
    tlast=`tail -1 tmp.txt`
    if [ $tlast -ge $tstart ]; then
	tstart=`expr $tlast + 1`
    fi
fi

for i in `seq ${tstart} ${step} ${tstop}`
do

#    echo $i
    tone=$i
    ttwo=`expr ${tone} + ${step}`
    
    if [ $tone -ge $tstop ]; then
	continue
    fi

    if [ $ttwo -ge $tstop ]; then
	ttwo=$tstop
    fi

    if [ $tone -le 1415088000 ]; then
	if [ $ttwo -ge 1415088000 ]; then
	    ttwo=1415088000
	fi
    fi
    
    tdateone=`date --utc --date=@$tone +%d-%m-%Y`
    ttimeone=`date --utc --date=@$tone +%H:%M`
    tdatetwo=`date --utc --date=@$ttwo +%d-%m-%Y`
    ttimetwo=`date --utc --date=@$ttwo +%H:%M`

    echo '  start: '`date --date=@$tone`'  stop: '`date --date=@$ttwo`
    echo "wget -O $1.$i.txt --post-data \"queryInfo=$2, element_name, $3, $tdateone $ttimeone, $tdatetwo $ttimetwo\" atlas-ddv.cern.ch:8089/multidata/downloadTxtData" >> command.log

    wget -O $1.$i.txt --post-data "queryInfo=$2, element_name, $3, $tdateone $ttimeone, $tdatetwo $ttimetwo" atlas-ddv.cern.ch:8089/multidata/downloadTxtData
    #wget -O $1.$i.txt --post-data "queryInfo=$2, element_name, $3, $tdateone $ttimeone, $tdatetwo $ttimetwo" pcaticstest04.cern.ch:8089/multidata/downloadTxtData
    rm -rf tmp.txt
    python ./convert.py $1.$i.txt > tmp.txt
    mv tmp.txt $1.$i.txt

done

rm -rf tmp.txt
cat $1.*txt | sort | uniq > tmp.txt
mv tmp.txt $1.txt
rm -rf $1.*.txt
