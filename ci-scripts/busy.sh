#!/usr/bin/env bash

run=$1

#rm file_Busy_run_$run.root
wget --no-check-certificate https://atlas-l1ct.cern.ch/busy/runs/$run/C/BUSY/file_Busy_run_$run.root

tnow=`date +%s`
tstart=1657018800 # starting on 05 July 2022
tstop=$tnow
./get_data.sh phys.Ready4Physics atlas_pvssdcs ATLGCSLHC:ATLAS.Ready4Physics $tstart $tstop
rm -rf command.log

timezone=`date +"%Z %z"`
gmtoffset=`date +"%z"`

offsetinput=${gmtoffset:2:1}

echo "Timzone information"
echo $timezone

root -b -q "busy.C("$run","$offsetinput")"
