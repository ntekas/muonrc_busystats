#!/usr/bin/env bash

runList=$1

fullRunList=$1
newRunList=$2
diffList=$3

#prune newRunList
sed -i '/diff/d' $newRunList
sed -i '/index/d' $newRunList
sed -i '/---/d' $newRunList
sed -i '/+++/d' $newRunList
sed -i '/@@/d' $newRunList
sed -i '/+/!d' $newRunList
sed -i 's/\(.\{1\}\)//' $newRunList

cat $newRunList
cat $diffList

if [ -s $newRunList ] && ! [ -s $diffList ]
then
  echo "Process new runs only"
  runList=$newRunList  
elif [ -s $diffList ]
then
  echo "Re-process all runs"
  runList=$fullRunList
else
  echo "No changes, nothing to (re-)process"
  exit 0
fi

while IFS= read -r line
do
  source ci-scripts/busy.sh $line
done < "$runList"
